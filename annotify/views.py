from utils import response
from django.http import HttpResponse
from utils.generate_token import generate_token as gen_token


def home(request, homepage):
    return response(request, homepage)

def generate_token(request):
    return HttpResponse(gen_token('madhav.bnk'), content_type='text/plain')
