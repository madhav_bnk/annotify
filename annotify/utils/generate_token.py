import datetime
import jwt
from django.conf import settings

# Only change this if you're sure you know what you're doing

ZERO = datetime.timedelta(0)
class Utc(datetime.tzinfo):
    def utcoffset(self, dt):
        return ZERO

    def tzname(self, dt):
        return "UTC"

    def dst(self, dt):
        return ZERO
UTC = Utc()

def generate_token(user_id):
    return jwt.encode({
      'consumerKey': settings.ANNOTATEIT_CONSUMER_KEY,
      'userId': user_id,
      'issuedAt': _now().isoformat(),
      'ttl': settings.ANNOTATEIT_CONSUMER_TTL
    }, settings.ANNOTATEIT_CONSUMER_SECRET)

def _now():
    return datetime.datetime.now(UTC).replace(microsecond=0)
