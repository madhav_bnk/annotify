from django.shortcuts import render_to_response
from django.template import RequestContext
from django.conf import settings


def response(request, template_name, params={}, content_type='text/html'):
    params.update(site_name=settings.PROJECT_NAME)
    return render_to_response(template_name, params,
                              context_instance=RequestContext(request),
                              content_type=content_type)